package com.ventas.lanix.ericktrejohdz.ventaslanix.utils;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ventas.lanix.ericktrejohdz.ventaslanix.R;

import java.util.List;

/**
 * Created by Erick on 21/08/17.
 */

public class MyRecycler extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private List<SectionOrRow> mData;
    private final MyRecycler.OnItemClickListener listener;

    public MyRecycler(List<SectionOrRow> data, MyRecycler.OnItemClickListener listener) {
        mData = data;
        this.listener = listener;
    }


    @Override
    public int getItemCount() {
        return mData.size();
    }


    @Override
    public int getItemViewType(int position) {
        super.getItemViewType(position);
        SectionOrRow item = mData.get(position);
        if(!item.isRow()) {
            return 0;
        } else {
            return 1;
        }
    }

    View rootView;
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType==0) {
            rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_section, parent, false);
            rootView.findViewById(R.id.tv_section);
            return new SectionViewHolder(rootView, listener);
        } else {
            rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_date, parent, false);
            return new RowViewHolder(rootView);
        }
    }

    private TextView textView;
    private TextView tvNombre;

    public class RowViewHolder extends RecyclerView.ViewHolder{

        public RowViewHolder(View itemView) {
            super(itemView);
            tvNombre = (TextView) rootView.findViewById(R.id.tv_nombre);
        }

        public void bind(final SectionOrRow item, final MyRecycler.OnItemClickListener listener) {

            rootView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onItemClick(item);
                }
            });
        }

    }


    public class SectionViewHolder extends RecyclerView.ViewHolder{
        public SectionViewHolder(View itemView, MyRecycler.OnItemClickListener listener) {
            super(rootView);
            textView = (TextView) rootView.findViewById(R.id.tv_section);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        SectionOrRow item = mData.get(position);
        if(item.isRow()) {
//            RowViewHolder h = (RowViewHolder) holder;
            tvNombre.setText(item.getNombre());
        } else {
//            SectionViewHolder h = (SectionViewHolder) holder;
            textView.setText(item.getSection());
        }
        bind(mData.get(position), listener);
    }

    public interface OnItemClickListener {
        void onItemClick(SectionOrRow item);

    }

    public void bind(final SectionOrRow item, final MyRecycler.OnItemClickListener listener) {

        rootView.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                listener.onItemClick(item);
            }
        });
    }


}