package com.ventas.lanix.ericktrejohdz.ventaslanix;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.ventas.lanix.ericktrejohdz.ventaslanix.utils.MenuItem;
import com.ventas.lanix.ericktrejohdz.ventaslanix.utils.MenuItemAdapter;

import com.ventas.lanix.ericktrejohdz.ventaslanix.views.ConsultaVentasActivity;
import com.ventas.lanix.ericktrejohdz.ventaslanix.views.LocationActivity;
import com.ventas.lanix.ericktrejohdz.ventaslanix.views.PromocionesActivity;
import com.ventas.lanix.ericktrejohdz.ventaslanix.views.AgendaActivity;
import com.ventas.lanix.ericktrejohdz.ventaslanix.views.RankingActivity;
import com.ventas.lanix.ericktrejohdz.ventaslanix.views.VentaActivity;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    TextView tvEntrada, tvSalida;
    RecyclerView rv_menu;
    String horaEntrada = "- - : - -";
    String horaSalida = "- - : - -";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initActionBar();

        SharedPreferences prefe = getSharedPreferences("datos", Context.MODE_PRIVATE);
        horaEntrada = prefe.getString("inicioLabores","- - : - -");
        horaSalida = prefe.getString("finLabores","- - : - -");

        initViews();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("TAG", "onResume: aaaaaaaaaaaaaaaaaaa");
        SharedPreferences prefe = getSharedPreferences("datos", Context.MODE_PRIVATE);
        horaEntrada = prefe.getString("inicioLabores","- - : - -");
        horaSalida = prefe.getString("finLabores","- - : - -");
        tvEntrada.setText(horaEntrada);
        tvSalida.setText(horaSalida);

    }

    @Override
    public boolean onSupportNavigateUp() {

        showCLoseAlert();

        return false;
    }

    private void initActionBar(){

//        this.getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("");
        View customNav = LayoutInflater.from(this).inflate(R.layout.layout_actionbar, null);
        getSupportActionBar().setDisplayShowCustomEnabled(true); // missing in your code
        getSupportActionBar().setCustomView(customNav);

        TextView btn = (TextView)customNav.findViewById(R.id.txt_auxbutton);
        btn.setText("CERRAR\nSESIÓN");
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showCLoseAlert();

            }
        });

    }

    private void showCLoseAlert(){

        AlertDialog.Builder dialog = new AlertDialog.Builder(MainActivity.this);
        dialog.setTitle(getString(R.string.app_name));
        dialog.setMessage("¿Deseas cerrar sesión?");

        dialog.setPositiveButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //Do nothing
            }
        } );

        dialog.setNegativeButton("SI", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        } );

        AlertDialog alert11 = dialog.create();
        alert11.show();

    }

    private void initViews(){

        tvEntrada   = (TextView)findViewById(R.id.tv_entrada_hora);
        tvSalida    = (TextView)findViewById(R.id.tv_salida_hora);
        tvEntrada.setText(horaEntrada);
        tvSalida.setText(horaSalida);

        rv_menu     = (RecyclerView)findViewById(R.id.rv_menu);
//        rv_menu.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        rv_menu.setLayoutManager(llm);
        MenuItemAdapter adapter = new MenuItemAdapter(initializeData(), new MenuItemAdapter.OnItemClickListener() {
            @Override public void onItemClick(MenuItem item) {

                if (item.getNextClass() != null)
                    changeView(item.getNextClass());
            }
        });


        rv_menu.setAdapter(adapter);
    }

    private void changeView(Class nextClass){

        Intent intent = new Intent(MainActivity.this, nextClass);
        startActivity(intent);
//        finish();

    }

    private ArrayList<MenuItem> initializeData(){

        ArrayList<MenuItem> itemsMenu = new ArrayList<>();

        itemsMenu.add(new MenuItem("ASISTENCIA",                LocationActivity.class));
        itemsMenu.add(new MenuItem("REGISTRO DE VENTA",         VentaActivity.class));
        itemsMenu.add(new MenuItem("CONSULTA DE VENTAS",        ConsultaVentasActivity.class));
        itemsMenu.add(new MenuItem("PROMOCIONES",               PromocionesActivity.class));

//        itemsMenu.add(new MenuItem("ASISTENCIA OLD",                AsistenciaActivity.class));
//        itemsMenu.add(new MenuItem("ASISTENCIA",                MyLocationDemoActivity.class));

//        itemsMenu.add(new MenuItem("AGENDA",                    AgendaActivity.class));

//        itemsMenu.add(new MenuItem("ANÁLISIS DE COMPETENCIA",   null));
//        itemsMenu.add(new MenuItem("RANKING",                   RankingActivity.class));

        return itemsMenu;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            showCLoseAlert();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
