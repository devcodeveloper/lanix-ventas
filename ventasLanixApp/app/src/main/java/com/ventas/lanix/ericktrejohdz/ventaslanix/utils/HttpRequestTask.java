package com.ventas.lanix.ericktrejohdz.ventaslanix.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by Janet Rivas on 2/19/17.
 */

public class HttpRequestTask extends AsyncTask<String, String, String> {

    private static final String TAG = "Http Request Task";

    public static final String REQUEST_TYPE_POST = "POST";
    public static final String REQUEST_TYPE_GET = "GET";

    HttpRequestTaskCallback callback;

    /**
     * param HttpRequestTaskCallback callback for when this task finishes
     * <br><br><b> - When calling execute, params as followed: </b><br>
     *  1. Url String<br>
     *  2. Request type (Defined in this class) REQUEST_TYPE_POST, REQUEST_TYPE_GET<br>
     *  3. (Optional) Data to be sent. <br>
     *  4. (Optional) Content Type  Default will be application/json<br>
     *  5. (Optional) Accept Type  default will be application/json
     *
     */
    public HttpRequestTask(HttpRequestTaskCallback hcb){
        this.callback = hcb;
    }


    @Override
    protected String doInBackground(String... params) {
        int length = params.length;
        String urlString = params[0];
        String request_type = params[1];

        //Grab and set data to be written if included
        String data;
        if(length>2){
            data = params[2];
        }else{
            data = null;
        }

        //Grab and set content type for the header if included
        String contentType;
        if(length>3){
            contentType = params[3];
        }else{
            contentType = "application/json";
        }
        //Grab and set accept type for the header if included
        String acceptType;
        if(length>4){
            acceptType = params[4];
        }else{
            acceptType = "application/json";
        }

        if(urlString == null || request_type == null){
            Log.e(TAG, "Can't process request, param error");
            if(callback!=null){
                callback.httpFailure(-1);
                callback = null;
            }
            return "Error";
        }

        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;

        try {
            URL url = new URL(urlString);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod(request_type);
            urlConnection.setRequestProperty("Content-Type", contentType);
            urlConnection.setRequestProperty("Accept", acceptType);
            urlConnection.setRequestProperty("Authorization", "Basic Og==");
            urlConnection.setUseCaches(false);
            urlConnection.setAllowUserInteraction(false);
            urlConnection.setConnectTimeout(100000);
            urlConnection.setReadTimeout(100000);

            if(request_type == REQUEST_TYPE_GET) {
                urlConnection.setRequestProperty("Content-length", "0");
                urlConnection.connect();
            }

            if(request_type == REQUEST_TYPE_POST){
                urlConnection.setDoInput(true);
                urlConnection.setDoOutput(true);

                OutputStream os = urlConnection.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                Log.d(TAG, "===> SENDING JSON: " + data);
                writer.write(data);
                writer.flush();
                writer.close();
                os.close();

            }

            int responseCode = urlConnection.getResponseCode();
            InputStream inputStream = urlConnection.getInputStream();

            Log.d(TAG, String.valueOf(responseCode) + " "+ inputStream);

            if (responseCode == HttpURLConnection.HTTP_OK) {

                StringBuffer buffer = new StringBuffer();
                if (inputStream == null) {
                    // Nothing to do.

                    return null;
                }
                reader = new BufferedReader(new InputStreamReader(inputStream));

                String inputLine;
                while ((inputLine = reader.readLine()) != null)
                    buffer.append(inputLine + "\n");
                if (buffer.length() == 0) {
                    // Stream was empty. No point in parsing.
                    return null;
                }

                String response = null;
                response = buffer.toString();


                return response;
            }else{
                if(callback!=null){
                    callback.httpFailure(responseCode);
                    callback = null;
                }
                Log.e(TAG, "Failed to download  response - " + responseCode);
                return null;
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (final IOException e) {
                    Log.d(TAG, "Error closing stream", e);
                }
            }

        }
        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        //Return on postExcewcute
        if(callback!=null){
            callback.httpCallComplete(result);
            callback = null;
        }
    }

    @Override
    protected void onCancelled() {

    }

    /**
     * Callback interface for HTTP requests.
     * s
     *
     */
    public interface HttpRequestTaskCallback{
        /**
         * Called when HTTP request is successfully completed.
         * @param response The response to the HTTP request.
         */
        public abstract void httpCallComplete(String response);

        /**
         * Called when HTTP request failed.
         * @param statusCode The HTTP failure code.
         */
        public abstract void httpFailure(int statusCode);
    }


    public static Bitmap downloadImage(String urlStr) throws IOException {
        URL url = new URL(urlStr);
        URLConnection connection = url.openConnection();
        BufferedInputStream bis = new BufferedInputStream(connection.getInputStream());
        Bitmap result = BitmapFactory.decodeStream(bis);
        bis.close();
        return result;
    }


    public static boolean sendRequest(final Context context, JSONObject bodyObject, String endpoint, String requestType, RequestCallback listCallback ){
        Log.d(TAG,"Checking to make sure we have a list");
        return sendRequest(context, null, listCallback, bodyObject, endpoint, requestType);
    }

    protected static boolean sendRequest(final Context context, HttpRequestTask.HttpRequestTaskCallback cb, final RequestCallback sentKeyCallback, JSONObject bodyObject, String endpoint, String requestType){
        if(context == null){
            return false;
        }

        if (cb == null) {
            cb = new HttpRequestTask.HttpRequestTaskCallback() {

                @Override
                public void httpCallComplete(String response) {
                    // Might want to check if this list is ok
                    Log.d(TAG, "APPS! " + response);
                    if(sentKeyCallback!=null){sentKeyCallback.onKeyRetrieved(response);}
                }

                @Override
                public void httpFailure(int statusCode) {
                    Log.d(TAG, "Error while requesting trusted app list: "
                            + statusCode);
                }
            };
        }

        new HttpRequestTask(cb).execute(endpoint, requestType, bodyObject.toString(), "application/json","application/json");

        return true;
    }

    /**
     * This interface is used as a callback to know when we have either obtained a list or at least returned from our attempt.
     *
     */
    public static interface RequestCallback{
        public void onKeyRetrieved(String response);
    }

}
