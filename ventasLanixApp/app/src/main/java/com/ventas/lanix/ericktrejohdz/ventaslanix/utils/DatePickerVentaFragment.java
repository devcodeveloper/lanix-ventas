package com.ventas.lanix.ericktrejohdz.ventaslanix.utils;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.icu.util.Calendar;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.widget.DatePicker;

import com.ventas.lanix.ericktrejohdz.ventaslanix.views.DetalleAgendaActivity;
import com.ventas.lanix.ericktrejohdz.ventaslanix.views.VentaActivity;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Erick on 21/08/17.
 */

public class DatePickerVentaFragment extends DialogFragment
        implements DatePickerDialog.OnDateSetListener{


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker

        final Calendar calendar;

        String input = getDateTimeNoTime();
        String[] out = input.split("/");
        System.out.println("Year = " + out[0]);
        System.out.println("Month = " + out[1]);
        System.out.println("Day = " + out[2]);

        int year =  Integer.parseInt(out[0]);
        int month = Integer.parseInt(out[1]);
        int day = Integer.parseInt(out[2]);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            calendar = Calendar.getInstance();
            year = calendar.get(Calendar.YEAR);
            month = calendar.get(Calendar.MONTH);
            day = calendar.get(Calendar.DAY_OF_MONTH);
        }

        // Create a new instance of DatePickerDialog and return it
        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {

        String date = String.valueOf(year)+"/"+ String.valueOf(month+1)+"/"+String.valueOf(day);
        Log.d("date", date);
        VentaActivity.setDate(date);
    }

    public static String getDateTimeNoTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy/MM/dd", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

}
