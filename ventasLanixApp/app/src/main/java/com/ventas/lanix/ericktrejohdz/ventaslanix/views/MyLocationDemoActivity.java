/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ventas.lanix.ericktrejohdz.ventaslanix.views;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMyLocationButtonClickListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.ventas.lanix.ericktrejohdz.ventaslanix.BannerActivity;
import com.ventas.lanix.ericktrejohdz.ventaslanix.MainActivity;
import com.ventas.lanix.ericktrejohdz.ventaslanix.R;
import com.ventas.lanix.ericktrejohdz.ventaslanix.SplashActivity;
import com.ventas.lanix.ericktrejohdz.ventaslanix.utils.PermissionUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * This demo shows how GMS Location can be used to check for changes to the users location.  The
 * "My Location" button uses GMS Location to set the blue dot representing the users location.
 * Permission for {@link android.Manifest.permission#ACCESS_FINE_LOCATION} is requested at run
 * time. If the permission has not been granted, the Activity is finished with an error message.
 */
public class MyLocationDemoActivity extends AppCompatActivity
        implements
        OnMyLocationButtonClickListener,
        OnMapReadyCallback,
        ActivityCompat.OnRequestPermissionsResultCallback {

    /**
     * Request code for location permission request.
     *
     * @see #onRequestPermissionsResult(int, String[], int[])
     */
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;

    /**
     * Flag indicating whether a requested permission has been denied after returning in
     * {@link #onRequestPermissionsResult(int, String[], int[])}.
     */
    private boolean mPermissionDenied = false;

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_location_demo);

        initViews();
        initActionBar();

        SupportMapFragment mapFragment =
                (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    private void initViews(){

        TextView tvDescription = (TextView) findViewById(R.id.tv_description);
        tvDescription.setText("ASISTENCIA");

        TextView tvDate = (TextView) findViewById(R.id.txtDate);
        Locale spanish = new Locale("es", "ES");
        Date anotherCurDate = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("EEEE, d 'de' MMMM\nHH:mm 'HRS'",spanish);
        final String formattedDateString = formatter.format(anotherCurDate).toUpperCase();
        tvDate.setText(formattedDateString);
        SimpleDateFormat formatterHour = new SimpleDateFormat("HH:mm 'HRS'",spanish);
        final String formattedDateHourString = formatterHour.format(anotherCurDate).toUpperCase();

        Button btnInicioLabores = (Button) findViewById(R.id.btnInicioLabores);
        btnInicioLabores.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences preferencias=getSharedPreferences("datos", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor=preferencias.edit();
                editor.putString("inicioLabores", formattedDateHourString);
                editor.putInt("habilitar", 2);
                editor.commit();
                Intent intent = new Intent(MyLocationDemoActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
        Button btnSalidaComida = (Button) findViewById(R.id.btnSalidaComida);
        btnSalidaComida.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences preferencias=getSharedPreferences("datos", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor=preferencias.edit();
                editor.putString("salidaComida", formattedDateHourString);
                editor.putInt("habilitar", 3);
                editor.commit();
                Intent intent = new Intent(MyLocationDemoActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
        Button btnRegresoComida = (Button) findViewById(R.id.btnRegresoComida);
        btnRegresoComida.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences preferencias=getSharedPreferences("datos", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor=preferencias.edit();
                editor.putString("regresoComida", formattedDateHourString);
                editor.putInt("habilitar", 4);
                editor.commit();
                Intent intent = new Intent(MyLocationDemoActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
        Button btnFinLabores = (Button) findViewById(R.id.btnFinLabores);
        btnFinLabores.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences preferencias=getSharedPreferences("datos", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor=preferencias.edit();
                editor.putString("finLabores", formattedDateHourString);
                editor.putInt("habilitar", 1);
                editor.commit();
                Intent intent = new Intent(MyLocationDemoActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

        SharedPreferences prefe=getSharedPreferences("datos", Context.MODE_PRIVATE);
        int habilitar = prefe.getInt("habilitar",1);

        switch (habilitar){

            case 1:
                btnInicioLabores.setEnabled(true);
                btnSalidaComida.setEnabled(true);
                btnRegresoComida.setEnabled(true);
                btnFinLabores.setEnabled(true);
                return;
            case 2:
                btnInicioLabores.setEnabled(false);
                btnSalidaComida.setEnabled(true);
                btnRegresoComida.setEnabled(true);
                btnFinLabores.setEnabled(true);
                return;
            case 3:
                btnInicioLabores.setEnabled(false);
                btnSalidaComida.setEnabled(false);
                btnRegresoComida.setEnabled(true);
                btnFinLabores.setEnabled(true);
                return;
            case 4:
                btnInicioLabores.setEnabled(false);
                btnSalidaComida.setEnabled(false);
                btnRegresoComida.setEnabled(false);
                btnFinLabores.setEnabled(true);
                return;

            default:
                return;
        }

    }

    private void initActionBar(){

//        this.getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("");
        View customNav = LayoutInflater.from(this).inflate(R.layout.layout_actionbar, null);
        getSupportActionBar().setDisplayShowCustomEnabled(true); // missing in your code
        getSupportActionBar().setCustomView(customNav);

        TextView btn = (TextView)customNav.findViewById(R.id.txt_auxbutton);
        btn.setText("");
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish(); // close this activity as oppose to navigating up

        return false;
    }

    @Override
    public void onMapReady(GoogleMap map) {
        mMap = map;

        mMap.setOnMyLocationButtonClickListener(this);
        enableMyLocation();

        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        String provider = locationManager.getBestProvider(criteria, true);
        Location location = mMap.getMyLocation();
        LatLng myPosition;


        if (location != null) {
            double latitude = location.getLatitude();
            double longitude = location.getLongitude();
            LatLng latLng = new LatLng(latitude, longitude);
            myPosition = new LatLng(latitude, longitude);


            LatLng coordinate = new LatLng(latitude, longitude);
            CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(coordinate, 19);
            mMap.animateCamera(yourLocation);
        }
    }

    /**
     * Enables the My Location layer if the fine location permission has been granted.
     */
    private void enableMyLocation() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission to access the location is missing.
//            PermissionUtils.requestPermission(this, LOCATION_PERMISSION_REQUEST_CODE,
//                    Manifest.permission.ACCESS_FINE_LOCATION, true);
        } else if (mMap != null) {
            // Access to the location has been granted to the app.
            mMap.setMyLocationEnabled(true);
        }
    }

    @Override
    public boolean onMyLocationButtonClick() {
        Toast.makeText(this, "MyLocation button clicked", Toast.LENGTH_SHORT).show();
        // Return false so that we don't consume the event and the default behavior still occurs
        // (the camera animates to the user's current position).
        return false;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
            @NonNull int[] grantResults) {
        if (requestCode != LOCATION_PERMISSION_REQUEST_CODE) {
            return;
        }
/*
        if (PermissionUtils.isPermissionGranted(permissions, grantResults,
                Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Enable the my location layer if the permission has been granted.
            enableMyLocation();
        } else {
            // Display the missing permission error dialog when the fragments resume.
            mPermissionDenied = true;
        }
*/    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        if (mPermissionDenied) {
            // Permission was not granted, display error dialog.
//            showMissingPermissionError();
            mPermissionDenied = false;
        }
    }

    /**
     * Displays a dialog with error message explaining that the location permission is missing.
     */
/*
    private void showMissingPermissionError() {
        PermissionUtils.PermissionDeniedDialog
                .newInstance(true).show(getSupportFragmentManager(), "dialog");
    }
*/
}
