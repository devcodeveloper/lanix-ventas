package com.ventas.lanix.ericktrejohdz.ventaslanix;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class BannerActivity extends AppCompatActivity {

    final int TIME = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_banner);
        Thread timerThread = new Thread() {
            public void run() {
                try {
                    sleep(TIME);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    BannerActivity.this.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                    Intent intent = new Intent(BannerActivity.this, LoginActivity.class);
                    startActivity(intent);

                }
            }
        };
        timerThread.start();
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        finish();
    }
}