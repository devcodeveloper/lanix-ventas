package com.ventas.lanix.ericktrejohdz.ventaslanix.utils;

import com.ventas.lanix.ericktrejohdz.ventaslanix.SplashActivity;

import java.util.List;

/**
 * Created by Erick on 20/08/17.
 */

public class MenuItem {

    String title;
    Class nextClass;

    public MenuItem(String title, Class nextClass){

        this.title      = title;
        this.nextClass  = nextClass;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Class getNextClass() {
        return nextClass;
    }

    public void setNextClass(Class nextClass) {
        this.nextClass = nextClass;
    }
}
