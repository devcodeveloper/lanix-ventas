package com.ventas.lanix.ericktrejohdz.ventaslanix.utils;

/**
 * Created by Erick on 24/09/17.
 */

public class Config {

    public static final String ENDPOINT    = "https://promotor.azurewebsites.net/Services.svc/";

    public static final String SEND_ALERT  = "sendAlert";      //send alert

    public static final String SEND_ALERT_phone     = "numTelefono";
    public static final String SEND_ALERT_user      = "nameUser";
    public static final String SEND_ALERT_latitude  = "latitud";
    public static final String SEND_ALERT_longitude = "longitud";
    public static final String SEND_ALERT_idDevice  = "deviceId";
    public static final String SEND_ALERT_message   = "msg";
    public static final String SEND_ALERT_date      = "date";




    public static final String MY_PREFERENCES_NAME              = "MisPrefeencias";
    public static final String PREFERENCES_USER_CLICKS          = "userClicks";
    public static final String PREFERENCES_USER_MESSAGE         = "userMessage";
    public static final String PREFERENCES_USER_NAME            = "userName";
    public static final String PREFERENCES_USER_PHONE           = "userPhone";
    public static final String PREFERENCES_USER_NAME_EMERGENCY  = "userNameEmergency";
    public static final String PREFERENCES_USER_PHONE_EMERGENCY = "userPhoneEmergency";

    public static final String PREFERENCES_FIRST_TIME           = "firtsTime";

}
