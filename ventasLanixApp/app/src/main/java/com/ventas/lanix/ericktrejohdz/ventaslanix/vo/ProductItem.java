package com.ventas.lanix.ericktrejohdz.ventaslanix.vo;

/**
 * Created by Erick on 04/10/17.
 */

public class ProductItem {

    String total;
    String model;
    String color;


    public ProductItem(String total, String model, String color) {
        this.total = total;
        this.model = model;
        this.color = color;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }


}
