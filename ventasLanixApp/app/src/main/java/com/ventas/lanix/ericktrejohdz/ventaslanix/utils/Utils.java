package com.ventas.lanix.ericktrejohdz.ventaslanix.utils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Erick on 24/09/17.
 */

public class Utils {

    public static JSONObject getJSONSendAlert(String user) {

        JSONObject jsonBody = new JSONObject();

        try {

            jsonBody.put(Config.SEND_ALERT_phone, "user.getPhone()");
            jsonBody.put(Config.SEND_ALERT_user, "user.getUser()");
            jsonBody.put(Config.SEND_ALERT_latitude, "user.getLatitude()");
            jsonBody.put(Config.SEND_ALERT_longitude, "user.getLongitude()");
            jsonBody.put(Config.SEND_ALERT_idDevice, "user.getIdDevice()");
            jsonBody.put(Config.SEND_ALERT_message, "user.getMessage()");
            jsonBody.put(Config.SEND_ALERT_date, "user.getDate()");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonBody;
    }
}
