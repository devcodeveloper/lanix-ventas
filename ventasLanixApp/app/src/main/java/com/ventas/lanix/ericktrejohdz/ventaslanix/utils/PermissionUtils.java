/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ventas.lanix.ericktrejohdz.ventaslanix.utils;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.ventas.lanix.ericktrejohdz.ventaslanix.R;
import com.ventas.lanix.ericktrejohdz.ventaslanix.views.LocationActivity;
import com.ventas.lanix.ericktrejohdz.ventaslanix.views.MyLocationDemoActivity;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Utility class for access to runtime permissions.
 */
public class PermissionUtils {
    private int MY_PERMISSIONS_REQUEST_READ_CONTACTS;
    public static String TAG = PermissionUtils.class.getSimpleName();
    private Context context;
    private LocationActivity current_activity;
    private PermissionResultCallback permissionResultCallback;
    private ArrayList<String> permission_list=new ArrayList<>();
    private ArrayList<String> listPermissionsNeeded=new ArrayList<>();

    String dialog_content="";
    int req_code;

    public PermissionUtils(LocationActivity context) {
        this.context=context;
        this.current_activity=  context;
        permissionResultCallback= (PermissionResultCallback) context;
    }

    public PermissionUtils(LocationActivity la, PermissionResultCallback callback) {
        this.current_activity    = la;
        this.context             = la.getBaseContext();
        permissionResultCallback = callback;
    }


    /**
     * Check the API Level & Permission
     *
     * @param permissions
     * @param dialog_content
     * @param request_code
     */

    public void check_permission(ArrayList<String> permissions, String dialog_content, int request_code) {
        Log.e(TAG, "check_permission() called with: permissions = [" + Arrays.toString(permissions.toArray()) + "], dialog_content = [" + dialog_content + "], request_code = [" + request_code + "]");
        this.permission_list = permissions;
        this.dialog_content  = dialog_content;
        this.req_code        = request_code;

        if(Build.VERSION.SDK_INT >= 23) {
            if (checkAndRequestPermissions(permissions, request_code)) {
                Log.i(TAG, "check_permission: all permissions");
                permissionResultCallback.PermissionGranted(request_code);
            }
        } else {
            permissionResultCallback.PermissionGranted(request_code);
        }

    }


    /**
     * Check and request the Permissions
     *
     * @param permissions
     * @param request_code
     * @return
     */

    private boolean checkAndRequestPermissions(ArrayList<String> permissions,int request_code) {
        Log.e(TAG, "checkAndRequestPermissions() called with: permissions = [" + permissions + "], request_code = [" + request_code + "]");
        if(permissions.size() > 0) {
            listPermissionsNeeded = new ArrayList<>();
            for(int i=0; i < permissions.size(); i++) {
                Log.w(TAG, "checkAndRequestPermissions: " );
                int hasPermission = ContextCompat.checkSelfPermission(current_activity,permissions.get(i));
                Log.e(TAG, "checkAndRequestPermissions: "+hasPermission+"  "+permissions.get(i) );
                Log.e(TAG, "checkAndRequestPermissions: "+PackageManager.PERMISSION_GRANTED );
                /**
                 * ¿El permiso a sido aprobado?
                 */
                if (hasPermission != PackageManager.PERMISSION_GRANTED) {
                    listPermissionsNeeded.add(permissions.get(i));
                }
            }

            if (!listPermissionsNeeded.isEmpty()) {
                Log.w(TAG, "checkAndRequestPermissions: list empty" );
                for (int i = 0; i < listPermissionsNeeded.size(); i++) {
                    // Here, thisActivity is the current activity
                    if (ContextCompat.checkSelfPermission(current_activity, listPermissionsNeeded.get(i)) != PackageManager.PERMISSION_GRANTED) {
                        // Should we show an explanation?
                        if (ActivityCompat.shouldShowRequestPermissionRationale(current_activity, listPermissionsNeeded.get(i))) {
                            Log.w(TAG, "requestPermission: this "+listPermissionsNeeded.get(i) );
                            // Show an expanation to the user *asynchronously* -- don't block
                            // this thread waiting for the user's response! After the user
                            // sees the explanation, try again to request the permission.

                        } else {
                            // No explanation needed, we can request the permission.
                            ActivityCompat.requestPermissions(current_activity, new String[]{listPermissionsNeeded.get(i)}, MY_PERMISSIONS_REQUEST_READ_CONTACTS);

                            // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                            // app-defined int constant. The callback method gets the
                            // result of the request.
                        }
                    }else{
                        Log.w(TAG, "requestPermission: PERMISSION GRANTED" );
                    }
                }
//                ActivityCompat.requestPermissions(current_activity, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),request_code);
                return false;
            }
        }

        return true;
    }

    /**
     *
     *
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        Log.d(TAG, "onRequestPermissionsResult() called with: requestCode = [" + requestCode + "], permissions = [" + permissions + "], grantResults = [" + grantResults + "]");
//        switch (requestCode)
//        {
//            case 1:
//                if(grantResults.length>0)
//                {
//                    Map<String, Integer> perms = new HashMap<>();
//
//                    for (int i = 0; i < permissions.length; i++)
//                    {
//                        perms.put(permissions[i], grantResults[i]);
//                    }
//
//                    final ArrayList<String> pending_permissions=new ArrayList<>();
//
//                    for (int i = 0; i < listPermissionsNeeded.size(); i++)
//                    {
//                        if (perms.get(listPermissionsNeeded.get(i)) != PackageManager.PERMISSION_GRANTED)
//                        {
//                            if(ActivityCompat.shouldShowRequestPermissionRationale(current_activity,listPermissionsNeeded.get(i)))
//                                pending_permissions.add(listPermissionsNeeded.get(i));
//                            else
//                            {
//                                Log.i("Go to settings","and enable permissions");
//                                permissionResultCallback.NeverAskAgain(req_code);
//                                Toast.makeText(current_activity, "Go to settings and enable permissions", Toast.LENGTH_LONG).show();
//                                return;
//                            }
//                        }
//
//                    }
//
//                    if(pending_permissions.size()>0)
//                    {
//                        showMessageOKCancel(dialog_content,
//                                new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialog, int which) {
//
//                                        switch (which) {
//                                            case DialogInterface.BUTTON_POSITIVE:
//                                                check_permission(permission_list,dialog_content,req_code);
//                                                break;
//                                            case DialogInterface.BUTTON_NEGATIVE:
//                                                Log.i("permisson","not fully given");
//                                                if(permission_list.size()==pending_permissions.size())
//                                                    permissionResultCallback.PermissionDenied(req_code);
//                                                else
//                                                    permissionResultCallback.PartialPermissionGranted(req_code,pending_permissions);
//                                                break;
//                                        }
//
//
//                                    }
//                                });
//
//                    }
//                    else
//                    {
//                        Log.i("all","permissions granted");
//                        Log.i("proceed","to next step");
//                        permissionResultCallback.PermissionGranted(req_code);
//
//                    }
//
//
//
//                }
//                break;
//        }
    }


    /**
     * Explain why the app needs permissions
     *
     * @param message
     * @param okListener
     */
    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new android.support.v7.app.AlertDialog.Builder(current_activity)
                .setMessage(message)
                .setPositiveButton("Ok", okListener)
                .setNegativeButton("Cancel", okListener)
                .create()
                .show();
    }

    public interface PermissionResultCallback
    {
        void PermissionGranted(int request_code);
        void PartialPermissionGranted(int request_code, ArrayList<String> granted_permissions);
        void PermissionDenied(int request_code);
        void NeverAskAgain(int request_code);
    }
}


