package com.ventas.lanix.ericktrejohdz.ventaslanix;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.ventas.lanix.ericktrejohdz.ventaslanix.views.AgendaActivity;
import com.ventas.lanix.ericktrejohdz.ventaslanix.views.AsistenciaActivity;
import com.ventas.lanix.ericktrejohdz.ventaslanix.views.DetalleVentaActivity;
import com.ventas.lanix.ericktrejohdz.ventaslanix.views.MyLocationDemoActivity;
import com.ventas.lanix.ericktrejohdz.ventaslanix.views.PromocionesActivity;
import com.ventas.lanix.ericktrejohdz.ventaslanix.views.VentaActivity;

public class SplashActivity extends AppCompatActivity {

    final int TIME = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Thread timerThread = new Thread() {
            public void run() {
                try {
                    sleep(TIME);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    Intent intent = new Intent(SplashActivity.this, BannerActivity.class);
//                    intent = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(intent);
                }
            }
        };
        timerThread.start();
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        finish();
    }
}