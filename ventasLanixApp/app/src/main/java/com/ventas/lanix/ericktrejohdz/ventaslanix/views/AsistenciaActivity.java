package com.ventas.lanix.ericktrejohdz.ventaslanix.views;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.ventas.lanix.ericktrejohdz.ventaslanix.BannerActivity;
import com.ventas.lanix.ericktrejohdz.ventaslanix.MainActivity;
import com.ventas.lanix.ericktrejohdz.ventaslanix.R;
import com.ventas.lanix.ericktrejohdz.ventaslanix.SplashActivity;
import com.ventas.lanix.ericktrejohdz.ventaslanix.utils.GPSTracker;

import java.text.SimpleDateFormat;
import java.util.Date;

public class AsistenciaActivity extends AppCompatActivity implements View.OnClickListener {

    TextView tvDescription;
    CardView cvEntrada, cvSalida, cvEntradaComida, cvSalidaComida;
    int REQUEST_LOCATION = 123;
    boolean gps_enabled = false;
    boolean network_enabled = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_asistencia);

        initActionBar();
        initViews();

        inirGPS();
        showInformation(isConnectedOrConnecting());

        // Acquire a reference to the system Location Manager
        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

// Define a listener that responds to location updates
        LocationListener locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                // Called when a new location is found by the network location provider.
                showInformation("ajale:::"+location.getLongitude()+":::"+location.getLatitude());
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {}

            public void onProviderEnabled(String provider) {}

            public void onProviderDisabled(String provider) {}
        };

// Register the listener with the Location Manager to receive location updates
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Check Permissions Now
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_LOCATION);
        } else {

        }
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_LOCATION) {
            if(grantResults.length == 1
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // We can now safely use the API we requested access to
                GPSTracker gps = new GPSTracker(this);
                if(gps.canGetLocation()){ // gps enabled} // return boolean true/false

                    Log.i("TAG", "onCreate: "+ gps.getLongitude()+":::"+gps.getLongitude());
                }
            } else {
                // Permission was denied or request was cancelled
            }
        }
    }

    private String isConnectedOrConnecting(){

        ConnectivityManager cm =
                (ConnectivityManager)getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
/*
        boolean isWiFi = activeNetwork.getType() == ConnectivityManager.TYPE_WIFI;
        boolean isWiFi = activeNetwork.getType() == ConnectivityManager.TYPE_ETHERNET;
        boolean isWiFi = activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE;
        boolean isWiFi = activeNetwork.getType() == ConnectivityManager.TYPE_BLUETOOTH;
        boolean isWiFi = activeNetwork.getType() == ConnectivityManager.TYPE_DUMMY;
        boolean isWiFi = activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE_DUN;
        boolean isWiFi = activeNetwork.getType() == ConnectivityManager.TYPE_VPN;
*/
        boolean conn = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

        return conn? "Si hay conexión a internet" : "No hay conexión a internet";
    }


    private void showInformation(String mensaje){

        Toast.makeText(getApplicationContext(),mensaje,Toast.LENGTH_LONG).show();
    }

    private void initViews(){

        tvDescription = (TextView)findViewById(R.id.tv_description);
        tvDescription.setText("ASISTENCIA");

        cvEntrada = (CardView)findViewById(R.id.cv_entrada);
        cvEntrada.setOnClickListener(this);
        cvEntradaComida = (CardView)findViewById(R.id.cv_entrada_comida);
        cvEntradaComida.setOnClickListener(this);
        cvSalida = (CardView)findViewById(R.id.cv_salida);
        cvSalida.setOnClickListener(this);
        cvSalidaComida = (CardView)findViewById(R.id.cv_salida_comida);
        cvSalidaComida.setOnClickListener(this);
    }

    private void initActionBar(){

//        this.getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("");
        View customNav = LayoutInflater.from(this).inflate(R.layout.layout_actionbar, null);
        getSupportActionBar().setDisplayShowCustomEnabled(true); // missing in your code
        getSupportActionBar().setCustomView(customNav);

        TextView btn = (TextView)customNav.findViewById(R.id.txt_auxbutton);
        btn.setText("");
    }

    @Override
    public boolean onSupportNavigateUp() {
        Intent intent = new Intent(AsistenciaActivity.this, MainActivity.class);
        startActivity(intent);
//        finish();

        return false;
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.cv_entrada:

                createSuccesAlert("Tu hora de entrada fué registrada correctamente", 1);

                break;
            case R.id.cv_entrada_comida:

                createSuccesAlert("Tu hora de regreso de comer fué registrada correctamente", 2);

                break;
            case R.id.cv_salida:

                createSuccesAlert("Tu hora de salida fué registrada correctamente", 3);

                break;
            case R.id.cv_salida_comida:

                createSuccesAlert("Tu hora de salida a comer fué registrada correctamente", 4);

                break;
        }

    }

    private void createSuccesAlert(String mensaje, final int horario){

        AlertDialog.Builder dialog = new AlertDialog.Builder(AsistenciaActivity.this);
        dialog.setTitle(getString(R.string.app_name));
        dialog.setMessage(mensaje);

        dialog.setPositiveButton("Enterado", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Intent intent = new Intent(AsistenciaActivity.this, MainActivity.class);

                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
                String currentDateandTime = sdf.format(new Date());
                Log.i("TAG", "onClick: "+currentDateandTime);

                if (horario==1)
                    intent.putExtra("horaEntrada",currentDateandTime);
                if (horario==3)
                    intent.putExtra("horaSalida",currentDateandTime);

                startActivity(intent);
                finish();
            }
        } );


        AlertDialog alert11 = dialog.create();
        alert11.show();
    }

    private void inirGPS(){

        LocationManager lm = (LocationManager) getApplicationContext()
                .getSystemService(Context.LOCATION_SERVICE);

        gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        Location net_loc = null, gps_loc = null, finalLoc = null;



        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Check Permissions Now
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_LOCATION);
        }

        if (gps_enabled)
            gps_loc = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if (network_enabled)
            net_loc = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

        if (gps_loc != null && net_loc != null) {

            //smaller the number more accurate result will
            if (gps_loc.getAccuracy() > net_loc.getAccuracy())
                finalLoc = net_loc;
            else
                finalLoc = gps_loc;

            // I used this just to get an idea (if both avail, its upto you which you want to take as I've taken location with more accuracy)

        } else {

            if (gps_loc != null) {
                finalLoc = gps_loc;
            } else if (net_loc != null) {
                finalLoc = net_loc;
            }
        }

        Log.i("TAG", "ujules::: "+ finalLoc.getLatitude()+":::"+finalLoc.getLongitude());

    }
}
