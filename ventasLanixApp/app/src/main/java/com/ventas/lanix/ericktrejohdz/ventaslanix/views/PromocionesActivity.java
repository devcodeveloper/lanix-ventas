package com.ventas.lanix.ericktrejohdz.ventaslanix.views;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.ventas.lanix.ericktrejohdz.ventaslanix.R;

public class PromocionesActivity extends AppCompatActivity {

    TextView tvDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promociones);

        initActionBar();
        initViews();

    }

    private void initViews(){

        tvDescription = (TextView)findViewById(R.id.tv_description);
        tvDescription.setText("PROMOCIONES");
    }

    private void initActionBar(){

//        this.getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("");
        View customNav = LayoutInflater.from(this).inflate(R.layout.layout_actionbar, null);
        getSupportActionBar().setDisplayShowCustomEnabled(true); // missing in your code
        getSupportActionBar().setCustomView(customNav);

        TextView btn = (TextView)customNav.findViewById(R.id.txt_auxbutton);
        btn.setText("");
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish(); // close this activity as oppose to navigating up

        return false;
    }

}
