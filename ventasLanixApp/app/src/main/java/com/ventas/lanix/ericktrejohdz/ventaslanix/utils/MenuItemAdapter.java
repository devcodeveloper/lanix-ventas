package com.ventas.lanix.ericktrejohdz.ventaslanix.utils;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ventas.lanix.ericktrejohdz.ventaslanix.R;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Erick on 20/08/17.
 */

public class MenuItemAdapter extends RecyclerView.Adapter<MenuItemAdapter.MenuItemHolder> {

    ArrayList<MenuItem> items;
    private final OnItemClickListener listener;

    public MenuItemAdapter(ArrayList<MenuItem> items, OnItemClickListener listener){
        this.items = items;
        this.listener = listener;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public MenuItemHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_menu, viewGroup, false);
        MenuItemHolder pvh = new MenuItemHolder(view,listener);
        return pvh;
    }

    @Override
    public void onBindViewHolder(MenuItemHolder itemViewHolder, int position) {
        itemViewHolder.title.setText(items.get(position).title);
        itemViewHolder.bind(items.get(position), listener);

    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public static class MenuItemHolder extends  RecyclerView.ViewHolder{

        CardView cv;
        TextView title;

        MenuItemHolder(View itemView, OnItemClickListener listener){
            super(itemView);

            cv = ((CardView)itemView.findViewById(R.id.cv));
            title = (TextView)itemView.findViewById(R.id.tv_titulo);
        }

        public void bind(final MenuItem item, final OnItemClickListener listener) {

            cv.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onItemClick(item);
                }
            });
        }

    }

    public interface OnItemClickListener {
        void onItemClick(MenuItem item);

    }


}
