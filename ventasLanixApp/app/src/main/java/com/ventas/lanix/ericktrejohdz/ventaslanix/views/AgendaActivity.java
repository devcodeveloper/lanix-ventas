package com.ventas.lanix.ericktrejohdz.ventaslanix.views;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.TextView;


import com.ventas.lanix.ericktrejohdz.ventaslanix.BannerActivity;
import com.ventas.lanix.ericktrejohdz.ventaslanix.R;
import com.ventas.lanix.ericktrejohdz.ventaslanix.SplashActivity;
import com.ventas.lanix.ericktrejohdz.ventaslanix.utils.ExpandableListAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class AgendaActivity extends AppCompatActivity {

    TextView tvDescription;

    ExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agenda);

        initActionBar();
        initViews();

    }

    private void initViews(){

        tvDescription = (TextView)findViewById(R.id.tv_description);
        tvDescription.setText("AGENDA");

        expListView = (ExpandableListView) findViewById(R.id.lvExp);
        prepareListData();

        listAdapter = new ExpandableListAdapter(
                AgendaActivity.this, listDataHeader, listDataChild);

        expListView.setAdapter(listAdapter);
        expListView.expandGroup(2);

        //expandAll();
        // Listview Group click listener
        expListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
                return false;
            }
        });

        expListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {}
        });

        expListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
            @Override
            public void onGroupCollapse(int groupPosition) {}
        });

        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {

                Intent intent = new Intent(AgendaActivity.this, DetalleAgendaActivity.class);
                intent.putExtra("datos", true);
                startActivity(intent);

                return false;
            }
        });

    }

    // method to expand all groups
    private void expandAll() {
        int count = listAdapter.getGroupCount();
        for (int i = 0; i < count; i++) {
            expListView.expandGroup(i);
        }
    }

    /*
     * Preparing the list data
     */
    private void prepareListData() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();

        // Adding child data
        listDataHeader.add("JUNIO");
        listDataHeader.add("JULIO");
        listDataHeader.add("AGOSTO");

        // Adding child data
        List<String> junio = new ArrayList<String>();
        junio.add("DANIELA RODRÍGUES\n04/06/2017\nPLAZA ANTARA");
        junio.add("LAURA HERNÁNDEZ\n13/06/2017\nPERIFÉRICO #2300");
        junio.add("MARIO TORRES\n24/06/2017\nOJO DE AGUA #34");
        junio.add("GERSON CALIXTO\n26/06/2017\nCASA OPULAR");

        List<String> julio = new ArrayList<String>();
        julio.add("GERARDO TREJO\n10/07/2017\nGRAN SUR");
        julio.add("MARIO TORRES\n14/07/2017\nOJO DE AGUA #34");
        julio.add("EDUARDO BARRDAS\n21/07/2017\nCUICUILCO");

        List<String> agosto = new ArrayList<String>();
        agosto.add("GERSON CALIXTO\n03/08/2017\nCASA OPULAR");
        agosto.add("JOSUE ALVARADO\n03/08/2017\nSAN JERÓNIMO #1200");
        agosto.add("MARIO TORRES\n14/08/2017\nOJO DE AGUA #34");
        agosto.add("EDUARDO BARRDAS\n21/08/2017\nCUICUILCO");
        agosto.add("GERSON CALIXTO\n24/08/2017\nCASA OPULAR");

        listDataChild.put(listDataHeader.get(0), junio);
        listDataChild.put(listDataHeader.get(1), julio);
        listDataChild.put(listDataHeader.get(2), agosto);
    }

    private void initActionBar(){

//        this.getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("");
        View customNav = LayoutInflater.from(this).inflate(R.layout.layout_actionbar, null);
        getSupportActionBar().setDisplayShowCustomEnabled(true); // missing in your code
        getSupportActionBar().setCustomView(customNav);

        TextView btn = (TextView)customNav.findViewById(R.id.txt_auxbutton);
        btn.setText("AGREGAR");
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AgendaActivity.this, DetalleAgendaActivity.class);
                intent.putExtra("datos", false);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish(); // close this activity as oppose to navigating up

        return false;
    }
}
