package com.ventas.lanix.ericktrejohdz.ventaslanix.utils;

/**
 * Created by Erick on 21/08/17.
 */

public class SectionOrRow {

    private String section;
    private boolean isRow;
    private String nombre;
    private String fecha;
    private String lugar;

    public static SectionOrRow createRow(String nombre, String fecha, String lugar) {
        SectionOrRow ret = new SectionOrRow();
        ret.isRow = true;
        ret.nombre = nombre;
        ret.fecha = fecha;
        ret.lugar = lugar;
        return ret;
    }

    public static SectionOrRow createSection(String section) {
        SectionOrRow ret = new SectionOrRow();
        ret.section = section;
        ret.isRow = false;
        return ret;
    }

    public String getSection() {
        return section;
    }

    public boolean isRow() {
        return isRow;
    }

    public String getNombre() {
        return nombre;
    }

    public String getFecha() {
        return fecha;
    }

    public String getLugar() {
        return lugar;
    }
}