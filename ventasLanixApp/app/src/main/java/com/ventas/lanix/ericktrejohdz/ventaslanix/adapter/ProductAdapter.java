package com.ventas.lanix.ericktrejohdz.ventaslanix.adapter;

/**
 * Created by Erick on 04/10/17.
 */

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ventas.lanix.ericktrejohdz.ventaslanix.R;
import com.ventas.lanix.ericktrejohdz.ventaslanix.vo.ProductItem;

import java.util.ArrayList;

public class ProductAdapter extends ArrayAdapter<ProductItem> {

//    ProductItem[] datos;
ArrayList<ProductItem> datos;
    public ProductAdapter(Context context, ArrayList<ProductItem> datos) {
        super(context, R.layout.row_product, datos);
        this.datos = datos;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View item = inflater.inflate(R.layout.row_product, null);

        TextView tvColor = (TextView) item.findViewById(R.id.row_tv_color);
        TextView tvModel = (TextView) item.findViewById(R.id.row_tv_model);
        TextView tvTotal = (TextView) item.findViewById(R.id.row_tv_total);

        tvColor.setText(datos.get(position).getColor());
        tvModel.setText(datos.get(position).getModel());
        tvTotal.setText(datos.get(position).getTotal());

        return(item);
    }
}
        /*
        extends ArrayAdapter<String> {
    private final Context context;
    ArrayList<ProductItem> producstItem;

    public ProductAdapter(Context context, String[] values, ArrayList<ProductItem> producstItem) {
        super(context, R.layout.row_product, values);
        this.context = context;
        this.producstItem = producstItem;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.row_product, parent, false);
        TextView tvColor = (TextView) rowView.findViewById(R.id.row_tv_color);
        TextView tvModel = (TextView) rowView.findViewById(R.id.row_tv_model);
        TextView tvTotal = (TextView) rowView.findViewById(R.id.row_tv_total);

        if (producstItem.size() > 0) {

            Log.i("TAG", "getView: "+producstItem.size());
            Log.i("TAG", "getView: "+producstItem.get(position).getColor());
            Log.i("TAG", "getView: "+producstItem.get(position).getModel());
            Log.i("TAG", "getView: "+producstItem.get(position).getTotal());

            tvColor.setText(producstItem.get(position).getColor());
            tvModel.setText(producstItem.get(position).getModel());
            tvTotal.setText(producstItem.get(position).getTotal());
        } else {

            tvColor.setText("");
            tvModel.setText("");
            tvTotal.setText("");
        }

        return rowView;
    }
}*/