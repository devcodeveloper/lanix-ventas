package com.ventas.lanix.ericktrejohdz.ventaslanix.views;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.annotation.RequiresPermission;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.ventas.lanix.ericktrejohdz.ventaslanix.R;
import com.ventas.lanix.ericktrejohdz.ventaslanix.utils.DatePickerFragment;

public class DetalleAgendaActivity extends AppCompatActivity {

    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 20314;
    TextView tvDescription;
    private static TextView selectDate;
    private Spinner spinnerProduct;
    TextView tv_telefono;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_agenda);

        initActionBar();
        initViews();

        Intent intent = getIntent();

        boolean datos = intent.getBooleanExtra("datos", false);
        if (datos) {

            TextView tv_nombre = (TextView) findViewById(R.id.tv_nombre);
            tv_nombre.clearFocus();
            tv_telefono = (TextView) findViewById(R.id.tv_telefono);
            tv_telefono.clearFocus();
            TextView tv_correo = (TextView) findViewById(R.id.tv_correo);
            tv_correo.clearFocus();
            TextView tv_ubicacion = (TextView) findViewById(R.id.tv_ubicacion);
            tv_ubicacion.clearFocus();

            tv_nombre.setText("DANIELA RODRÍGUEZ");
            tv_telefono.setText("55 3245 2940");
            tv_correo.setText("ericktrejohdz@gmail.com");
            tv_ubicacion.setText("AV IMAN 580");
            tv_nombre.clearFocus();
            tv_telefono.clearFocus();
            tv_correo.clearFocus();
            tv_ubicacion.clearFocus();

            tv_nombre.setSelected(false);
            tv_telefono.setSelected(false);
            tv_correo.setSelected(false);
            tv_ubicacion.setSelected(false);
        }
    }

    private void initViews() {

        tvDescription = (TextView) findViewById(R.id.tv_description);
        tvDescription.setText("DANIELA RODRÍGUEZ");
        selectDate = (TextView) findViewById(R.id.date);
        selectDate.setText("01/10/2017");
        spinnerProduct = (Spinner) findViewById(R.id.product);

        String estatus[] = {"ESTATUS", "TERMINADA", "NUEVA CITA", "VENTA REALIZADA", "SIN RESPUESTA"};

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, estatus);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        spinnerProduct.setAdapter(spinnerArrayAdapter);

        Button btnMaps = (Button) findViewById(R.id.btn_mapa);
        btnMaps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri gmmIntentUri = Uri.parse("geo:37.7749,-122.4194");
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);
            }
        });
        RelativeLayout rlMapa = (RelativeLayout) findViewById(R.id.rl_mapa);
        rlMapa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri gmmIntentUri = Uri.parse("geo:37.7749,-122.4194");
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);
            }
        });

        RelativeLayout rlMail = (RelativeLayout)findViewById(R.id.rl_correo);
        rlMail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    Log.i("Send email", "");
                    String[] TO = {"ericktrejohdz@gmail.com"};
                    String[] CC = {""};
                    Intent emailIntent = new Intent(Intent.ACTION_SEND);

                    emailIntent.setData(Uri.parse("mailto:"));
                    emailIntent.setType("text/plain");
                    emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
                    emailIntent.putExtra(Intent.EXTRA_CC, CC);
                    emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Hola");
                    emailIntent.putExtra(Intent.EXTRA_TEXT, "Texto de prueba!");

                    try {
                        startActivity(Intent.createChooser(emailIntent, "Enviar mail..."));
                    } catch (android.content.ActivityNotFoundException ex) {


                    }
            }
        });
        Button btn_aceptar = (Button) findViewById(R.id.btn_aceptar);
        btn_aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });

        RelativeLayout rlLlamar = (RelativeLayout) findViewById(R.id.rl_llamar);
        rlLlamar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.i("TAG", "onClick: erick");

                if (ContextCompat.checkSelfPermission(DetalleAgendaActivity.this,
                        Manifest.permission.READ_CONTACTS)
                        != PackageManager.PERMISSION_GRANTED) {

                    // Should we show an explanation?
                    if (ActivityCompat.shouldShowRequestPermissionRationale(DetalleAgendaActivity.this,
                            Manifest.permission.READ_CONTACTS)) {

                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + tv_telefono.getText().toString()));
                        startActivity(intent);

                        // Show an expanation to the user *asynchronously* -- don't block
                        // this thread waiting for the user's response! After the user
                        // sees the explanation, try again to request the permission.

                    } else {

                        // No explanation needed, we can request the permission.

                        ActivityCompat.requestPermissions(DetalleAgendaActivity.this,
                                new String[]{Manifest.permission.CALL_PHONE},
                                MY_PERMISSIONS_REQUEST_READ_CONTACTS);

                        // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                        // app-defined int constant. The callback method gets the
                        // result of the request.
                    }
                } else {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + tv_telefono.getText().toString()));
                    if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    startActivity(intent);

                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_CONTACTS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {


                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + tv_telefono.getText().toString()));
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    startActivity(intent);

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    private void initActionBar(){

//        this.getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("");
        View customNav = LayoutInflater.from(this).inflate(R.layout.layout_actionbar, null);
        getSupportActionBar().setDisplayShowCustomEnabled(true); // missing in your code
        getSupportActionBar().setCustomView(customNav);

        TextView btn = (TextView)customNav.findViewById(R.id.txt_auxbutton);
        btn.setText("");
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish(); // close this activity as oppose to navigating up

        return false;
    }

    public void showDatePickerDialog(View v) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    public  static void setDate(String date){
        date = date;
        selectDate.setText(date);

    }

    private void go(View v){

        Uri gmmIntentUri = Uri.parse("geo:37.7749,-122.4194?z=10&q=restaurants");
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        startActivity(mapIntent);
    }
}
