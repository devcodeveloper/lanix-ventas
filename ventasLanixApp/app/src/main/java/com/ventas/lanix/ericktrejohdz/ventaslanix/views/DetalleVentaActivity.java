package com.ventas.lanix.ericktrejohdz.ventaslanix.views;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.ventas.lanix.ericktrejohdz.ventaslanix.MainActivity;
import com.ventas.lanix.ericktrejohdz.ventaslanix.R;
import com.ventas.lanix.ericktrejohdz.ventaslanix.utils.Movie;
import com.ventas.lanix.ericktrejohdz.ventaslanix.utils.MoviesAdapter;

import java.util.ArrayList;
import java.util.List;

public class DetalleVentaActivity extends AppCompatActivity {

    private List<Movie> movieList = new ArrayList<>();
    private RecyclerView recyclerView;
    private MoviesAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_venta);
        initActionBar();
        initViews();
    }


    private void initViews() {

        TextView tvDescription = (TextView) findViewById(R.id.tv_description);
        tvDescription.setText("DETALLE DE VENTA");

        Button btnConfirmar = (Button) findViewById(R.id.btn_confirmar);
        btnConfirmar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder dialog = new AlertDialog.Builder(DetalleVentaActivity.this);
                dialog.setTitle(getString(R.string.app_name));
                dialog.setMessage("Tu venta ha sido registrada satisfactoriamente");

                dialog.setPositiveButton("Enterado", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent = new Intent(DetalleVentaActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                } );


                AlertDialog alert11 = dialog.create();
                alert11.show();
            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        mAdapter = new MoviesAdapter(movieList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        prepareMovieData();
    }

    private void prepareMovieData() {


        Movie movie = new Movie("ALL-IN-ONE", "AIO 215", "24 UNIDADES");
        movieList.add(movie);

        movie = new Movie("LAPTOP", "NEUTRON A", "12 UNIDADES");
        movieList.add(movie);

        movie = new Movie("LAPTOP", "NEUTRON G6", "23 UNIDADES");
        movieList.add(movie);

        movie = new Movie("LAPTOP", "NEUTRON V", "45 UNIDADES");
        movieList.add(movie);

        movie = new Movie("LAPTOP", "NEUTRON FLEX", "3 UNIDADES");
        movieList.add(movie);

        movie = new Movie("LAPTOP", "Action & Adventure", "33 UNIDADES");
        movieList.add(movie);

        movie = new Movie("SMARTPHONE", "L620", "3 UNIDADES");
        movieList.add(movie);

        movie = new Movie("SMARTPHONE", "LT520", "2015");
        movieList.add(movie);

        movie = new Movie("SMARTPHONE", "L610", "4 UNIDADES");
        movieList.add(movie);

        movie = new Movie("SMARTPHONE", "L1100", "45 UNIDADES");
        movieList.add(movie);

        movie = new Movie("SMARTPHONE", "L910", "22 UNIDADES");
        movieList.add(movie);

        movie = new Movie("TABLET", "E8", "56 UNIDADES");
        movieList.add(movie);


        mAdapter.notifyDataSetChanged();
    }
    private void initActionBar(){

//        this.getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("");
        View customNav = LayoutInflater.from(this).inflate(R.layout.layout_actionbar, null);
        getSupportActionBar().setDisplayShowCustomEnabled(true); // missing in your code
        getSupportActionBar().setCustomView(customNav);

        TextView btnAux = (TextView)customNav.findViewById(R.id.txt_auxbutton);
        btnAux.setText("EDITAR");
    }

    @Override
    public boolean onSupportNavigateUp() {
        Intent intent = new Intent(DetalleVentaActivity.this, VentaActivity.class);
        startActivity(intent);
        finish();

        return false;
    }

}
