package com.ventas.lanix.ericktrejohdz.ventaslanix.views;

import android.annotation.TargetApi;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.SurfaceTexture;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.ventas.lanix.ericktrejohdz.ventaslanix.MainActivity;
import com.ventas.lanix.ericktrejohdz.ventaslanix.R;
import com.ventas.lanix.ericktrejohdz.ventaslanix.SplashActivity;
import com.ventas.lanix.ericktrejohdz.ventaslanix.adapter.ProductAdapter;
import com.ventas.lanix.ericktrejohdz.ventaslanix.utils.DatePickerFragment;
import com.ventas.lanix.ericktrejohdz.ventaslanix.utils.DatePickerVentaFragment;
import com.ventas.lanix.ericktrejohdz.ventaslanix.vo.ProductItem;

import java.io.ByteArrayInputStream;
import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.ImageFormat;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.ImageReader;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Size;
import android.util.SparseIntArray;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import butterknife.OnItemSelected;

@TargetApi(Build.VERSION_CODES.LOLLIPOP)
@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class VentaActivity extends AppCompatActivity {

    TextView tvDescription;
    TextView tvPhoto;
    private static TextView selectDate;
    TextView btnAux;
    int i = 0;
    private static final String TAG = "AndroidCameraApi";

    int totalPhotos = 0;

    ArrayList<ProductItem> arrayProducts;
//    ProductItem[] datos;

    ProductAdapter productAdapter;
    ListView lv_products;
    ProductAdapter adaptador;

    String alertColor = "", alertModel = "", alertTotal = "";

    private static final SparseIntArray ORIENTATIONS = new SparseIntArray();
    static {
        ORIENTATIONS.append(Surface.ROTATION_0, 90);
        ORIENTATIONS.append(Surface.ROTATION_90, 0);
        ORIENTATIONS.append(Surface.ROTATION_180, 270);
        ORIENTATIONS.append(Surface.ROTATION_270, 180);
    }

//    private TextureView textureView;

    private String cameraId;
    protected CameraDevice cameraDevice;
    protected CameraCaptureSession cameraCaptureSessions;
    protected CaptureRequest captureRequest;
    protected CaptureRequest.Builder captureRequestBuilder;
    private Size imageDimension;
    private ImageReader imageReader;
    private File file;
    private static final int REQUEST_CAMERA_PERMISSION = 200;
    private boolean mFlashSupported;
    private Handler mBackgroundHandler;
    private HandlerThread mBackgroundThread;
    TextView tvCantidad;

    AlertDialog alertDialog;

    Spinner spn_color, spn_model, spn_product;
    TextView tvCantidadAlert;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_venta);

        initActionBar();
        initViews();

//        showAddProductAlert();
    }


    public static int TAKE_IMAGE = 111;
    Uri mCapturedImageURI;

    private void initViews() {

        tvDescription = (TextView) findViewById(R.id.tv_description);
        tvDescription.setText("AGREGAR VENTA");

        tvPhoto = (TextView) findViewById(R.id.tv_photo);
        tvPhoto.setText(getTakenPhotos());

        selectDate = (TextView) findViewById(R.id.date);
        selectDate.setText("01/10/2017");

        Spinner spn_cliente = (Spinner) findViewById(R.id.spn_cliente);
        String clientes[] = {"SELECCIONAR CLIENTE","DANIELA RODRIGUEZ","GERARDO TREJO","COPPEL","LAURA HERNÁNDEZ","MARIO TORRES"};
        ArrayAdapter<String> spinnerArrayAdapterClientes = new ArrayAdapter<String>(this,   android.R.layout.simple_spinner_item, clientes);
        spinnerArrayAdapterClientes.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        spn_cliente.setAdapter(spinnerArrayAdapterClientes);

        Button btnOtroProducto = (Button) findViewById(R.id.btn_otro_producto);
        btnOtroProducto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                showAddProductAlert();
//                takePicture();

//                Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
//                intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(Environment.getExternalStorageDirectory()+"/LANIX/pic.jpg")));
//                startActivityForResult(intent, 11);
            }
        });

        Button btn_aceptar = (Button) findViewById(R.id.btn_aceptar);
        btn_aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(VentaActivity.this, MainActivity.class);
                startActivity(intent);
                finish();

            }
        });
/*
        textureView = (TextureView) findViewById(R.id.texture);
        assert textureView != null;
        textureView.setSurfaceTextureListener(textureListener);
*/
        ImageView mImg = (ImageView) findViewById(R.id.iv_photo);
        mImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    String fileName = "temp.jpg";
                    ContentValues values = new ContentValues();
                    values.put(MediaStore.Images.Media.TITLE, fileName);
                    mCapturedImageURI = getContentResolver()
                            .insert(
                                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                                    values);
                    Intent intent = new Intent(
                            MediaStore.ACTION_IMAGE_CAPTURE);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT,
                            mCapturedImageURI);
                    startActivityForResult(intent, TAKE_IMAGE);
                } catch (Exception e) {
                    Log.e("", "", e);
                }
            }
        });

        arrayProducts = new ArrayList<ProductItem>();
//        arrayProducts.add(new ProductItem("Título 1", "Subtítulo largo 1","pp"));

        adaptador =
                new ProductAdapter(this, arrayProducts);

        lv_products = (ListView) findViewById(R.id.lv_products);

        lv_products.setAdapter(adaptador);



    }

    private class StableArrayAdapter extends ArrayAdapter<String> {

        HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();

        public StableArrayAdapter(Context context, int textViewResourceId,
                                  List<String> objects) {
            super(context, textViewResourceId, objects);
            for (int i = 0; i < objects.size(); ++i) {
                mIdMap.put(objects.get(i), i);
            }
        }

        @Override
        public long getItemId(int position) {
            String item = getItem(position);
            return mIdMap.get(item);
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

    }

    private void setArrayColor(int position, View viewInflated){

        Spinner spn_color = (Spinner) viewInflated.findViewById(R.id.spn_color);

        ArrayList<String> colores = new ArrayList<String>();

        switch (position) {
            case 0:
                colores.add("SELECCIONAR COLOR");

            break;
            case 1:
                colores.add("NEGRO");
                colores.add("GRIS");
                colores.add("ROJO");
                break;
            case 2:
                colores.add("BLANCO");
                colores.add("AZUL");
                colores.add("NEGRO");
                colores.add("GRIS");
                colores.add("ROJO");
                break;
            case 3:
                colores.add("BLANCO");
                colores.add("NEGRO");
                break;
            case 4:
                colores.add("GRIS");
                colores.add("PLATA");
                break;
            case 5:
                colores.add("ROSA");
                colores.add("DORADO");
                colores.add("GRIS");
                break;
            case 6:
                colores.add("NEGRO");
                colores.add("GRIS");
                colores.add("ROSA");
                colores.add("DORADO");
                break;
            case 7:
                colores.add("NEGRO");
                colores.add("ROSA");
                colores.add("DORADO");
                break;
            case 8:
                colores.add("NEGRO");
                colores.add("ROSA");
                colores.add("DORADO");
                break;
            case 9:
                colores.add("NEGRO");
                colores.add("ROSA");
                colores.add("DORADO");
                break;
        }
        ArrayAdapter<String> spinnerArrayAdapterColores = new ArrayAdapter<String>(this,   android.R.layout.simple_spinner_item, colores);
        spinnerArrayAdapterColores.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        spn_color.setAdapter(spinnerArrayAdapterColores);

    }

    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent intent) {

        if ((requestCode == VentaActivity.TAKE_IMAGE)
                && (resultCode == RESULT_OK)) {

            totalPhotos++;
            tvPhoto.setText(getTakenPhotos());

            String[] projection = { MediaStore.Images.Media.DATA };
            Cursor cursor = managedQuery(mCapturedImageURI, projection, null,
                    null, null);
            int column_index_data = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();

            String capturedImageFilePath = cursor.getString(column_index_data);
            Bitmap bitmap = BitmapFactory.decodeFile(capturedImageFilePath);
            float angle = 90;
            final Drawable rotatedDrawable = getRotateDrawable(bitmap,angle);

            ImageView mImg;
            mImg = (ImageView) findViewById(R.id.iv_photo);

//            mImg.setImageDrawable(rotatedDrawable);

        }
    }

    Drawable getRotateDrawable(final Bitmap b, final float angle) {
        final BitmapDrawable drawable = new BitmapDrawable(getResources(), b) {
            @Override
            public void draw(final Canvas canvas) {
                canvas.save();
                canvas.rotate(angle, b.getWidth() / 2, b.getHeight() / 2);
                super.draw(canvas);
                canvas.restore();
            }
        };
        return drawable;
    }

    private void initActionBar(){

//        this.getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("");
        View customNav = LayoutInflater.from(this).inflate(R.layout.layout_actionbar, null);
        getSupportActionBar().setDisplayShowCustomEnabled(true); // missing in your code
        getSupportActionBar().setCustomView(customNav);

        btnAux = (TextView)customNav.findViewById(R.id.txt_auxbutton);
        btnAux.setText("TOTAL\n"+i);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish(); // close this activity as oppose to navigating up

        return false;
    }

    public void showDatePickerDialog(View v) {
        DialogFragment newFragment = new DatePickerVentaFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    public  static void setDate(String date){
        date = date;
        selectDate.setText(date);

    }

    private String getTakenPhotos(){

        return String.valueOf(totalPhotos);
    }

    private void showAddProductAlert(){

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Agregar producto");
        // I'm using fragment here so I'm using getView() to provide ViewGroup
        // but you can provide here any other instance of ViewGroup from your Fragment / Activity
        View viewInflated = LayoutInflater.from(this).inflate(R.layout.add_product_alert, (ViewGroup) findViewById(android.R.id.content), false);

        initAlertView(viewInflated);
        builder.setView(viewInflated);

        alertDialog = builder.show();
    }

    private void initAlertView(final View viewInflated){

        spn_product = (Spinner) viewInflated.findViewById(R.id.spn_product);
        String productos[] = {"SELECCIONAR PRODUCTO","SMARTPHONE"};
        ArrayAdapter<String> spinnerArrayAdapterProductos = new ArrayAdapter<String>(this,   android.R.layout.simple_spinner_item, productos);
        spinnerArrayAdapterProductos.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        spn_product.setAdapter(spinnerArrayAdapterProductos);
        spn_product.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // your code here
                Log.i(TAG, "onItemSelected: "+position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        spn_model = (Spinner) viewInflated.findViewById(R.id.spn_model);
        String modelos[] = {"SELECCIONAR MODELO", "U100", "X120", "X220", "X520", "X710", "LT520", "L620", "L920", "L1120"};
        final ArrayAdapter<String> spinnerArrayAdapterModelos = new ArrayAdapter<String>(this,   android.R.layout.simple_spinner_item, modelos);
        spinnerArrayAdapterModelos.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        spn_model.setAdapter(spinnerArrayAdapterModelos);
        spn_model.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // your code here
                Log.i(TAG, "onItemSelected: "+position);
                setArrayColor(position, viewInflated);
                alertModel = spinnerArrayAdapterModelos.getItem(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
        spn_color = (Spinner) viewInflated.findViewById(R.id.spn_color);
        String colores[] = {"SELECCIONAR COLOR", "NEGRO", "GRIS", "ROJO"};
        final ArrayAdapter<String> spinnerArrayAdapterColores = new ArrayAdapter<String>(this,   android.R.layout.simple_spinner_item, colores);
        spinnerArrayAdapterColores.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        spn_color.setAdapter(spinnerArrayAdapterColores);
        spn_color.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // your code here
                Log.i(TAG, "onItemSelected: "+position);
                alertColor = spinnerArrayAdapterColores.getItem(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        tvCantidad = (TextView) viewInflated.findViewById(R.id.tv_cantidad);

        Button btnAcept = (Button) viewInflated.findViewById(R.id.btn_alert_acept);
        btnAcept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                i++;
                btnAux.setText("TOTAL\n"+i);

                ProductItem tr = new ProductItem( tvCantidad.getText().toString(), spn_model.getSelectedItem().toString(), spn_color.getSelectedItem().toString());
                arrayProducts.add(tr);
                adaptador.notifyDataSetChanged();
                alertDialog.dismiss();
            }
        });

        Button btnCancel = (Button) viewInflated.findViewById(R.id.btn_alert_cancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(alertDialog != null && alertDialog.isShowing()){
                    alertDialog.dismiss();
                }
            }
        });

    }

}
