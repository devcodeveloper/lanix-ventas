package com.ventas.lanix.ericktrejohdz.ventaslanix.views;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityCompat.OnRequestPermissionsResultCallback;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.beardedhen.androidbootstrap.TypefaceProvider;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.ventas.lanix.ericktrejohdz.ventaslanix.MainActivity;
import com.ventas.lanix.ericktrejohdz.ventaslanix.R;
import com.ventas.lanix.ericktrejohdz.ventaslanix.utils.Config;
import com.ventas.lanix.ericktrejohdz.ventaslanix.utils.HttpRequestTask;
import com.ventas.lanix.ericktrejohdz.ventaslanix.utils.LocationHelper;
import com.ventas.lanix.ericktrejohdz.ventaslanix.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LocationActivity extends AppCompatActivity implements ConnectionCallbacks,
                                                                OnConnectionFailedListener,
                                                                OnRequestPermissionsResultCallback,
                                                                OnMapReadyCallback {
    private LocationHelper locationHelper;
    // LogCat tag
    private static final String TAG = LocationActivity.class.getSimpleName();

    private final static int PLAY_SERVICES_REQUEST = 1000;
    private final static int REQUEST_CHECK_SETTINGS = 2000;

    private Location mLastLocation;
    // Google client to interact with Google API
    private double latitude;
    private double longitude;
    private GoogleMap mapa;

    // list of permissions
    private String hora = "", direccion = "";
    public int MY_PERMISSION_LOCATION = 1;
    /**
     * NO remover etiquetas @
     */
    @BindView(R.id.btn_location)   Button rlPick;
    @BindView(R.id.tv_coordenates) TextView tvCoordenates;
    @BindView(R.id.txtDate)        TextView tvDate;
    private int MY_PERMISSIONS_REQUEST_READ_CONTACTS;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TypefaceProvider.registerDefaultIconSets();
        Log.d(TAG, "onCreate() called with: savedInstanceState = [" + savedInstanceState + "]");
        setContentView(R.layout.activity_location_alan);
        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        ButterKnife.bind(this);

        initActionBar();
        Locale spanish             = new Locale("es", "ES");
        Date anotherCurDate        = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("EEEE, d 'de' MMMM\nHH:mm 'HRS'",spanish);
        this.locationHelper        = new LocationHelper(this);
//        this.locationHelper.checkPlayServices();
//        locationHelper             = new LocationHelper(this);
        String formattedDateString = formatter.format(anotherCurDate).toUpperCase();
        tvDate.setText(formattedDateString);
        hora           = formattedDateString;
        mapFragment.getMapAsync(this);
        rlPick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                refreshLocation();
            }
        });

//        requestPermission();
//        new CountDownTimer(30000, 1000) {
//
//            public void onTick(long millisUntilFinished) {
//                refreshLocation();
//            }
//
//            public void onFinish() {
//                showToast("Tiempo de sincronización finalizado");
//            }
//        }.start();

/*
        btnProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showToast("Proceed to the next step");
            }
        });
*/
        // check availability of play services
//        if (locationHelper.checkPlayServices()) {
//
//            // Building the GoogleApi client
//            locationHelper.buildGoogleApiClient();
//        }

    }



    public void refreshLocation(){
        mLastLocation  = locationHelper.getLocation();
        if (mLastLocation != null) {
            latitude  = mLastLocation.getLatitude();
            longitude = mLastLocation.getLongitude();
            Log.w(TAG, "onClick: LAT: "+latitude+"   LON: "+longitude );
            getAddress();
        } else {
//            showToast("No hay sensores encendidos, recalculando...");
            locationHelper.mGoogleApiClient = null;
        }
    }


    @Override
    public void onMapReady(GoogleMap map) {
        mapa = map;
    }

    public void getAddress() {
        Log.d(TAG, "getAddress() called");
        Address locationAddress;
        locationAddress=locationHelper.getAddress(latitude,longitude);
        tvCoordenates.setText("Latitud:  "+latitude +" Longitud:  "+longitude+"\n");
        if(locationAddress!=null){
            Log.w(TAG, "onClick: LAT: "+locationAddress.getLatitude()+"   LON: "+locationAddress.getLongitude() );
            String address = locationAddress.getAddressLine(0);
            String address1 = locationAddress.getAddressLine(1);
            String city = locationAddress.getLocality();
            String state = locationAddress.getAdminArea();
            String country = locationAddress.getCountryName();
            String postalCode = locationAddress.getPostalCode();
            String currentLocation;

            if(!TextUtils.isEmpty(address)) {
                currentLocation=address;

                direccion = address;
                if (!TextUtils.isEmpty(address1))
                    currentLocation+="\n"+address1;

                if (!TextUtils.isEmpty(city))
                {
                    currentLocation+="\n"+city;

                    if (!TextUtils.isEmpty(postalCode))
                        currentLocation+=" - "+postalCode;
                }
                else
                {
                    if (!TextUtils.isEmpty(postalCode))
                        currentLocation+="\n"+postalCode;
                }

                if (!TextUtils.isEmpty(state))
                    currentLocation+="\n"+state;

                if (!TextUtils.isEmpty(country))
                    currentLocation+="\n"+country;

                final LatLng Marker = new LatLng(latitude, longitude);


                mapa.addMarker(new MarkerOptions()
                        .position(new LatLng(latitude, longitude))
                        .title(address1));

                LatLng userLocation = new LatLng(latitude, longitude);

                CameraPosition camPos = new CameraPosition.Builder()
                        .target(userLocation)
                        .zoom(15)         //Establecemos el zoom en 19
                        .bearing(0)      //Establecemos la orientación con el noreste arriba
                        .tilt(0)         //Bajamos el punto de vista de la cámara 70 grados
                        .build();

                CameraUpdate camUpd3 = CameraUpdateFactory.newCameraPosition(camPos);
                mapa.animateCamera(camUpd3);
                initViews();
            }

        } else {
            showToast("Cargando información de sensores...");
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        locationHelper.onActivityResult(requestCode,resultCode,data);
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (locationHelper != null){
            locationHelper.checkPlayServices();
        }else{
            Log.e(TAG, "onResume: locationHelper");
        }
    }

    /**
     * Google api callback methods
     */
    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Log.i("Connection failed:", " ConnectionResult.getErrorCode() = "
                + result.getErrorCode());
    }

    @Override
    public void onConnected(Bundle arg0) {

        // Once connected with google api, get the location
        Log.d(TAG, "onConnected() called with: arg0 = [" + arg0 + "]");
        refreshLocation();
//        mLastLocation=locationHelper.getLocation();
    }

    @Override
    public void onConnectionSuspended(int arg0) {
        locationHelper.connectApiClient();
    }

    public void showToast(String message)
    {
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show();
    }

    private void initViews(){

        TextView tvDescription = (TextView) findViewById(R.id.tv_description);
        tvDescription.setText("ASISTENCIA");

        Locale spanish = new Locale("es", "ES");
        Date anotherCurDate = new Date();

        SimpleDateFormat formatterHour = new SimpleDateFormat("HH:mm 'HRS'",spanish);
        final String formattedDateHourString = formatterHour.format(anotherCurDate).toUpperCase();

        BootstrapButton btnInicioLabores = (BootstrapButton) findViewById(R.id.btnInicioLabores);
        btnInicioLabores.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences preferencias=getSharedPreferences("datos", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor=preferencias.edit();
                editor.putString("inicioLabores", formattedDateHourString);
                editor.putInt("habilitar", 2);
                editor.commit();
                showConfirmAlert();
//                Intent intent = new Intent(LocationActivity.this, MainActivity.class);
//                startActivity(intent);
//                finish();
            }
        });
        BootstrapButton btnSalidaComida = (BootstrapButton) findViewById(R.id.btnSalidaComida);
        btnSalidaComida.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences preferencias=getSharedPreferences("datos", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor=preferencias.edit();
                editor.putString("salidaComida", formattedDateHourString);
                editor.putInt("habilitar", 3);
                editor.commit();
                showConfirmAlert();
//                Intent intent = new Intent(LocationActivity.this, MainActivity.class);
//                startActivity(intent);
//                finish();
            }
        });
        BootstrapButton btnRegresoComida = (BootstrapButton) findViewById(R.id.btnRegresoComida);
        btnRegresoComida.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences preferencias=getSharedPreferences("datos", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor=preferencias.edit();
                editor.putString("regresoComida", formattedDateHourString);
                editor.putInt("habilitar", 4);
                editor.commit();
                showConfirmAlert();
//                Intent intent = new Intent(LocationActivity.this, MainActivity.class);
//                startActivity(intent);
//                finish();
            }
        });
        BootstrapButton btnFinLabores = (BootstrapButton) findViewById(R.id.btnFinLabores);
        btnFinLabores.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences preferencias=getSharedPreferences("datos", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor=preferencias.edit();
                editor.putString("finLabores", formattedDateHourString);
                editor.putInt("habilitar", 1);
                editor.commit();
                showConfirmAlert();
//                Intent intent = new Intent(LocationActivity.this, MainActivity.class);
//                startActivity(intent);
//                finish();
            }
        });

        SharedPreferences prefe=getSharedPreferences("datos", Context.MODE_PRIVATE);
        int habilitar = prefe.getInt("habilitar",1);

        switch (habilitar){

            case 1:
                btnInicioLabores.setEnabled(true);
                btnSalidaComida.setEnabled(true);
                btnRegresoComida.setEnabled(true);
                btnFinLabores.setEnabled(true);
                return;
            case 2:
                btnInicioLabores.setEnabled(false);
                btnSalidaComida.setEnabled(true);
                btnRegresoComida.setEnabled(true);
                btnFinLabores.setEnabled(true);
                return;
            case 3:
                btnInicioLabores.setEnabled(false);
                btnSalidaComida.setEnabled(false);
                btnRegresoComida.setEnabled(true);
                btnFinLabores.setEnabled(true);
                return;
            case 4:
                btnInicioLabores.setEnabled(false);
                btnSalidaComida.setEnabled(false);
                btnRegresoComida.setEnabled(false);
                btnFinLabores.setEnabled(true);
                return;

            default:
                return;
        }

    }

    private void initActionBar(){

//        this.getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("");
        View customNav = LayoutInflater.from(this).inflate(R.layout.layout_actionbar, null);
        getSupportActionBar().setDisplayShowCustomEnabled(true); // missing in your code
        getSupportActionBar().setCustomView(customNav);

        TextView btn = (TextView)customNav.findViewById(R.id.txt_auxbutton);
        btn.setText("");
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish(); // close this activity as oppose to navigating up

        return false;
    }


    private void showConfirmAlert(){

        AlertDialog.Builder dialog = new AlertDialog.Builder(LocationActivity.this);
        dialog.setTitle("Confirmar datos");
        dialog.setMessage(hora + "\n" + direccion);

        dialog.setPositiveButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //Do nothing
            }
        } );

        dialog.setNegativeButton("SI", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                sendAlert();
            }
        } );

        AlertDialog alert11 = dialog.create();
        alert11.show();

    }


    private void requestPermission(){
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                Log.w(TAG, "requestPermission: this, Manifest.permission.ACCESS_FINE_LOCATION" );
                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_READ_CONTACTS);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }else{
            Log.w(TAG, "requestPermission: PERMISSION GRANTED" );
            refreshLocation();
        }
    }
    // Permission check functions
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        Log.e(TAG, "onRequestPermissionsResult() called with: requestCode = [" + requestCode + "], permissions = [" + permissions + "], grantResults = [" + grantResults + "]");
        this.locationHelper        = new LocationHelper(this);
//        switch (requestCode) {
//            case 1: {
//                // If request is cancelled, the result arrays are empty.
//                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    Log.w(TAG, "onRequestPermissionsResult: grantResults.length" );
//                    locationHelper.onRequestPermissionsResult(requestCode,permissions,grantResults);
//
//                } else {
//                    // permission denied, boo! Disable the
//                    // functionality that depends on this permission.
//                    Log.w(TAG, "onRequestPermissionsResult: else {" );
//                }
//                return;
//            }
//            // other 'case' lines to check for other
//            // permissions this app might request
//        }
    }


    private void sendAlert(){

        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

        String endpoint = Config.ENDPOINT + Config.SEND_ALERT;

        finish();
//        JSONObject jsonBody = Utils.getJSONSendAlert(userInformation);
//        callService(jsonBody, endpoint);
    }

    private void callService(JSONObject jsonBody, String endpoint){

        HttpRequestTask.sendRequest(this.getApplicationContext(), jsonBody, endpoint, HttpRequestTask.REQUEST_TYPE_POST, new HttpRequestTask.RequestCallback(){
            @Override
            public void onKeyRetrieved(String response) {

                Log.i(TAG, "onKeyRetrieved: "+response);

                Intent intent = new Intent(LocationActivity.this, MainActivity.class);
                startActivity(intent);
                finish();

                JSONObject jsonObj = null;
                try {
                    if(response != null) {
                        jsonObj = new JSONObject(response);

                        String codeMessage = jsonObj.getString("codeMessage");
                        if(codeMessage.equals("0")){


                        } else {

                        }
                    } else {

                        response = "{\"codeMessage\": \"0\", \"message\": \"Alerta enviada satisfactoriamente\"}";
//                        response = "{\"codeMessage\": \"-1\", \"message\": \"Ocurrio un error, intentalo mas tarde\"}";

                        jsonObj = new JSONObject(response);

                        String codeMessage = jsonObj.getString("codeMessage");
                        if(codeMessage.equals("0")){

                        } else {

                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }


}
